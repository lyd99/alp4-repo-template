; nasm -f elf64 fib.asm ; gcc -std=c11 -Wall -Wextra -pedantic -O2 -o prog fib.c fib.o
; ./prog ZAHL

; Joachim Bufler, David Ly (5331962)

GLOBAL asm_fib_it
GLOBAL asm_fib_rek




asm_fib_it:
fibit:
		MOV rax, rdi
		MOV r9, 0	;x = 0
		MOV rcx, 1	;y = 1
		MOV rdx, 0	;k = 0			rax > 0
		MOV r8, rax 	;Zwischenspeicher
		CMP rax, 0
		JA .while
		
.while: 
		MOV r9, rcx	;x=y
		MOV rcx, rdx	;y=k
		ADD r9, rcx	;k=x+y
		MOV rdx, r9	;rdx=k
		DEC r8		;
		CMP r8, 0	;
		JA .while	;
		JMP .endit	;
		
.endit:
		
		MOV rax,rdx
		RET
		
;###############
;###Rekursion###
;###############




asm_fib_rek:
fibrek:
		PUSH rbx
		MOV rax, rdi
		CMP rdi, 2   ;used to be 1
		JB .end
		MOV rbx, rdi
		SUB rdi, 1

		CALL fibrek
		MOV rdi, rbx
		MOV rbx, rax
		SUB rdi, 2
		CALL fibrek
		ADD rax, rbx
		

.end:
		POP rbx
		RET
