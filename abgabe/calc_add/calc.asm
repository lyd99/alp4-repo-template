;nasm -f elf64 -g -F dwarf calc_add.asm; gcc -std=c11 -Wall -Wextra -pedantic -O2 -o prog calc.c calc_add.o
;gdb prog
;break calc_add
;set disassembly-flavor intel
;run 7.0 7.0
;layout reg
;ni

; Joachim Bufler, David Ly (5331962)

GLOBAL calc_add

SECTION .text

calc_add:
	
	PUSH r12
	PUSH r13
	PUSH r14
	PUSH r15	
	
	XOR r8,r8
	XOR r9,r9
	XOR r10,r10
	XOR r11,r11
	XOR r12,r12
	XOR r12,r12
	XOR r12,r12
	XOR r13,r13
	XOR r14,r14
	XOR r15,r15
	XOR edx, edx

	MOVD eax, XMM0
	MOVD ebx, XMM1
	

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

;Operand Nr.1

_VZNum1:

	MOV r8d, eax	;r8 VZ
	SHR r8d, 31
	
_CharNum1:
	
	MOV r9d, eax
	SHL r9d, 1
	SHR r9d, 24
	
	
_MantissaNum1:
	
	MOV r10d, eax
	SHL r10d, 9

;;;;;;;;;;;;;;;;;;;;;;

;Operand Nr.2

_VZNum2:
	MOV r11d, eax	;r11d VZ
	SHR r11d, 31

_CharNum2:
	
	MOV r12d, eax
	SHL r12d, 1
	SHR r12d, 24

_MantissaNum2:
	
	MOV r13d, eax
	SHL r13d, 9

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

;Normalisierung
	XOR r14, r14
	MOV r14, 8388608	;1000.0000.0000.0000.0000.0000
	OR r10d, r14d
	OR r13d, r14d

	CALL _cmpChar
	MOV [rdi], edx		;Ergebnis

	XOR r12, r12
  	XOR r13, r13
  	XOR r14, r14
  	XOR r15, r15
  	POP r15     
  	POP r14      
  	POP r13      
  	POP r12  

	RET

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

;Vergleiche Charakteristik

_cmpChar:

	CMP r9d, r12d
	JE _cmpVZ
	JL _angleichen1
	JA _angleichen2

_angleichen1:
	
	INC r9d
	SHR r10d, 1
	JMP _cmpChar

_angleichen2:
	
	INC r12d
	SHR r13d, 1
	JMP _cmpChar


_cmpVZ:

	CMP r8d, r11d	
	JE _calculation	; Wenn OP1, OP2 gleiches vorzeichen haben
	JNE _ungleich	; Wenn OP1 positiv und OP2 negativ ungleichen Vorzeichen

_ungleich:

;Differenz der beiden Charakteristiken feststellen
	
	CMP r10d, r13d
	JL _zweierkomplement1
	JA _zweierkomplement2
	JE _zweierkomplement1

_zweierkomplement1:
	
	MOV r15, "OP1"		;OP1 ist kleiner
	XOR r14, r14
	MOV r14d, 4294967295	; 1111.1111.1111.1111.1111.1111.1111.1111
	XOR r10d, r14d
	ADD r10d, 1
	JMP _calculation
	
_zweierkomplement2:
	
	MOV r15, "OP2"		;OP2 ist kleiner
	XOR r14, r14
	MOV r14d, 4294967295
	XOR r13d, r14d
	ADD r13d, 1
	JMP _calculation

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

;Berechnung

_calculation:

	ADD r10d, r13d
	
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

;Denormalisierung

	XOR r14, r14
	MOV r14d, r10d
	SHR r14d, 23
	JMP _denormalisierung 

_denormalisierung:
	
	CMP r14d, 1
	JE _signToResult	; Zahl vor der Mantisse =1
	JA _fall1		; Zahl vor der Mantisse >1
	JL _fall2		; Zahl vor der Mantisse <1

_fall1:

	SHR r14d, 1
	SHR r10d, 1
	INC r9d
	JMP _denormalisierung

_fall2:

	CMP r10d, 0
	JE _ende
	SHL r10d, 1
	DEC r9d
	XOR r14, r14
	MOV r14d, r10d
	SHR r14d, 23
	JMP _denormalisierung

_ende:

	RET

_signToResult:

	CMP r15, "OP1"		;Vergleiche ob OP1 die kleine Zahl ist. Falls OP1 gr��er ist dann nehme VZ. Falls kleiner nehme VZ von OP2
	JNE _VZ1
	JE _VZ2

_VZ1:

	JMP _end

_VZ2:

	XOR r8, r8
	MOV r8d, r11d
	JMP _end

_end:
	
	SHL r10d, 9
	SHR r10d, 9
	OR edx, r10d

	SHL r9, 23
	OR edx, r9d
	
	SHL r8d, 31
	OR edx, r8d

	XOR rax, rax
	MOV eax, edx

	RET
